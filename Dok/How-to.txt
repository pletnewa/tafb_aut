1. Задача: добавить новой свойство в проект. Например имя пользователя, пароль или URL странички.
Короткий путь:
1. Открываем файл resources/func_tests.properties;
2. Прописываем новое свойство в виде "ключ=значение", например
 createBookPage=localhost:8080/createBook
где createBookPage это ключ;
3. Идём туда, где нам нужно это значение и берём его через PropertiesHolder:
 PropertiesHolder.getProperties().getProperty("createBookPage");

Длинный путь, более "программистский" и более предпочтительный:
1. Добавляем нужное в resources/func_tests.properties;
2. Открываем resources/func_tests.config.xml;
3. Добавляем строчку в соответствующий бин, URL в urls, остальное можно в data
 <property name="pageBookCreate" value="${page.book.create}"/>
4. Идём в класс utils/data/DataHolder(если добавляем всяко разное) или в utils/data/URLsHolder(добавляем URL);
5. Добавляем приватное поле по имени совпадающее(это важно) с тем, что было прописано в бине в атрибуте namе:
 private String pageBookCreate;
6. Добавляем/генерируем публичные getter и setter для этого поля;
7. После этого новое поле доступно через соответствующий Holder:
 URLsHolder.getHolder().getPageBookCreate()

=======================================================================