package le.tafb.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * A Singlenton container for WebDriver object.</br>
 * http://en.wikipedia.org/wiki/Singleton_pattern
 */
public class Browser {
    private static final Logger logger = LoggerFactory.getLogger(Browser.class);
    private static WebDriver driver;
    private static Browser browser;

    private Browser(){
        //TODO: use .properties to specify what browser to use.
        driver = new FirefoxDriver();

        //Set timeouts to 60 seconds. //TODO: specify timeouts in the .properties file for easy deploying.
        int timeout = 60;
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeout, TimeUnit.SECONDS);
    }

    public static Browser getBrowser() {
        if (null == browser){
            logger.info("Initializing new browser.");
            browser = new Browser();
        }
        return browser;
    }

    /**
     * Gets the "driver" object. Not the best idea to expose WebDriver object here, but need this to illustrate how PageFactory pattern works.
     *
     * @return the "driver" object.
     */
    public static WebDriver getDriver() {
        return driver;
    }

    /**
     * Close the browser.
     *
     */
    public static void quit() {
        logger.info("Closing the browser.");
        getDriver().quit();
    }

    /**
     * Gets URL of the current page.
     *
     * @return URL of the current page.
     */
    public static String getCurrentUrl(){
        return driver.getCurrentUrl();
    }

    /**
     * Gets title of the current page.
     *
     * @return title of the current page.
     */
    public static String getPageTitle(){
        return driver.getTitle();
    }
}