package le.tafb.utils.helpers;

import le.tafb.utils.Browser;
import le.tafb.utils.tools.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class, a wrapper for underlying library(ies) calls. Provides unified approach to accessing UI components.
 * //TODO: Introduce waitForElement... calls in methods like find*, select* etc.
 * //Decouple from Selenuim's By.Introduce own wrapper for By.
 */
public class UiHelper {
    private static final Logger logger = LoggerFactory.getLogger(UiHelper.class);

    //TODO: Make configurable.
    protected static final int WAITFOR_TIMEOUT = 32; // seconds

    private static final int DEFAULT_SLEEP_TIMEOUT = 1000; // milliseconds

    /**
     * Pauses test execution for specified amount of time.
     *
     * @param milliseconds time to sleep, in milliseconds
     */
    public static void sleep(int milliseconds) {
        try {
            logger.debug("sleep() for" + milliseconds);
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            Log.logException("---> Thread terminated during sleep.", e);
        }
    }

    /**
     * Sleep for default time specified in DEFAULT_SLEEP_TIMEOUT constant
     */
    public static void sleep() {
        sleep(DEFAULT_SLEEP_TIMEOUT);
    }

    /**
     * Waits for an element .
     * //TODO: Provide waitFor(By by, long timeout)
     *
     * @param by element's locator.
     * @return indicates if an element is found
     */
    public static boolean waitFor(By by) {
        logger.debug("waitFor() " + by);

        WebElement element = (new WebDriverWait(Browser.getDriver(), WAITFOR_TIMEOUT))
                .until(ExpectedConditions.presenceOfElementLocated(by));
        if (null == element){
            Log.logWarning(String.format("---> WaitFor '%s' was not successful", by));

            return false;
        }

        return true;
    }

    /**
     * Yet another "waitFor". Just an example of own ExpectedCondition.
     *
     * @param xpath	XPATH that distinguish an element
     * @return indicates if an element has been found.
     */
    public static boolean waitFor(final String xpath) {
        logger.debug("waitFor() " + xpath);

        return (new WebDriverWait(Browser.getDriver(), WAITFOR_TIMEOUT)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                WebElement element = Browser.getDriver().findElement(By.xpath(xpath));
                return element.isDisplayed();
            }
        });
    }

    /**
     * Simple wrapper around driver.findElement().
     * Simplifies logging and extending.
     *
     * @param by - element-to-find locator.
     *
     * @return WebElement found.
     */
    public static WebElement findElement(By by) {
        logger.debug("findElement() By: " + by.toString());

        return Browser.getDriver().findElement(by);
    }

    /**
     * Finds element by the locator specified and left-mouse clicks it.
     *
     * @param by - element's locator.
     */
    public static void click(By by) {
        WebElement element = findElement(by);

        logger.debug("click() element By: " + by.toString());
        element.click();
    }

    /**
     * Left-clicks in the middle of the given element and holds mouse button down.
     *
     * @param by - element's locator.
     */
    public static void clickAndHold(By by) {
        WebElement element = findElement(by);

        logger.debug("Clicking and holding element By: " + by.toString());
        getWebDriverActions().clickAndHold(element).perform();
    }

    /**
     * Releases left mouse button.
     */
    public static void release() {
        logger.debug("Releasing left mouse button.");
        getWebDriverActions().release().perform();
    }

    /**
     * Checks whether element specified by the locator is present on current page.
     *
     * @param by - locator of the element.
     *
     * @return True if the element present on the page.
     */
    public static boolean isPresent(By by) {
        logger.debug("isPresent() By: " + by.toString());

        return Browser.getDriver().findElements(by).size() > 0;
    }

    /**
     * Checks whether element specified by id is present on current page.
     *
     * @param id - element's id.
     *
     * @return True if the element present on the page.
     */
    public static boolean isPresentById(String id) {
        logger.debug("isPresentById(): " + id);
        return isPresent(By.id(id));
    }

    /**
     * Sends keys to the element specified.
     *
     * @param element - target element.
     * @param value string to be typed.
     */
    public static void sendKeys(WebElement element, CharSequence value) {
        logger.debug(String.format("Sending Keys '%s' to %s ",value.toString(), element.toString()));
        element.sendKeys(value);
    }

    /**
     * Finds and sends keys to the element specified by locator.
     *
     * @param by - target element's locator.
     * @param value string to be typed.
     */
    public static void sendKeys(By by, CharSequence value) {
        sendKeys(findElement(by), value);
    }

    /**
     * Selects {value} in the element specified by locator.
     * The element is supposed to be a <select/>.
     *
     * @param by - element's locator.
     * @param value - value to select.
     */
    public static void selectByValue(By by, String value) {
        logger.debug(String.format("Selecting value %s in %s.", value, by));
        new Select(findElement(by)).selectByValue(value);
    }

    /**
     * Selects an option by displayed text in the element specified by locator.
     * The element is supposed to be a <select/>.
     *
     * @param by - element's locator.
     * @param txt - text value to use upon the selection.
     */
    public static void selectByText(By by, String txt) {
        logger.debug(String.format("Selecting text %s in %s.", txt, by));
        new Select(findElement(by)).selectByVisibleText(txt);
    }

    /**
     * Tries to raise submit on the element.
     * @param element - the target element.
     */
    public static void submit(WebElement element) {
        logger.debug("Submitting " + element);
        element.submit();
    }

    /**
     * Tries to raise submit on element specified by the locator.
     *
     * @param by- the target element's locator.
     */
    public static void submit(By by) {
        submit(findElement(by));
    }

    /**
     * Gets element's text if the element can be found.
     *
     * @param by - element-to-find-and-derive-text-from locator.
     *
     * @return WebElement's text.
     */
    public static String getText(By by) {
        WebElement el = findElement(by);
        if (null != el){
            return el.getText();
        }

        logger.error(String.format("Element(%s) Not Found, cannot get its text.", by));

        return "{NOT FOUND, NOTHING TO GET, SEE THE LOG.}";
    }

    /**
     * Gets element attribute's value if the element can be found.
     *
     * @param by - element-to-find-and-get-attribute-value-from locator.
     * @param attributeName - attribute-to-use name.
     *
     * @return WebElement's attribute value.
     */
    public static String getAttributeValue(By by, String attributeName) {
        WebElement el = findElement(by);
        if (null != el){
            return el.getAttribute(attributeName);
        }

        logger.error(String.format("Element(%s) Not Found, cannot get its attribute(%s) value.", by, attributeName));

        return "{NOT FOUND, NOTHING TO GET, SEE THE LOG.}";
    }

    //=====================================================================
    /**
     * Gets the actions for the active driver/</br>
     * Note: "Actions" is WebDriver specific thing, so this could break upon switching from WebDriver to something else.
     *
     * @return   WebDriver's Actions.
     */
    private static Actions getWebDriverActions() {
        return new Actions(Browser.getDriver());
    }
}