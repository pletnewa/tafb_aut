package le.tafb.utils.entities;

import le.tafb.utils.entities.enums.Honorific;
import le.tafb.utils.tools.Util;

import java.util.Date;

/**
 * Represents "User" entity of the AUT.
 */
public class TestUser {

    private Honorific honorific;

    private String loginName;

    private String firstUserName;

    private String middleUserName;

    private String lastUserName;

    private int age;

    private boolean isActive = false;

    private String password;

    private Date birthDate;

    /**
     * TestUser entity constructor. All required fields.
     * 'isActive' is false by default.
     *
     * @param loginName - user's login name.
     * @param firstUserName - user's first name.
     * @param lastUserName - last user name.
     * @param password - user's password.
     * @param birthDate - user's birth date as Date.
     */
    public TestUser(String loginName, String firstUserName, String lastUserName, String password, Date birthDate) {
        this.loginName = loginName;
        this.firstUserName = firstUserName;
        this.lastUserName = lastUserName;
        this.password = password;
        this.birthDate = birthDate;
    }

    /**
     * TestUser entity constructor. All required fields. BirthDate - as string.
     * 'isActive' is false by default.
     *
     * @param loginName - user's login name.
     * @param firstUserName - user's first name.
     * @param lastUserName - last user name.
     * @param password - user's password.
     * @param birthDateAsString - user's birth date as String. Should use Constants.DATE_FORMAT_FORM_DATES format.
     */
    public TestUser(String loginName, String firstUserName, String lastUserName, String password, String birthDateAsString) {
        this.loginName = loginName;
        this.firstUserName = firstUserName;
        this.lastUserName = lastUserName;
        this.password = password;
        this.birthDate = Util.convertToFormDate(birthDateAsString);
    }

    public Honorific getHonorific() {
        return honorific;
    }

    public void setHonorific(Honorific honorific) {
        this.honorific = honorific;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getFirstUserName() {
        return firstUserName;
    }

    public void setFirstUserName(String firstUserName) {
        this.firstUserName = firstUserName;
    }

    public String getMiddleUserName() {
        return middleUserName;
    }

    public void setMiddleUserName(String middleUserName) {
        this.middleUserName = middleUserName;
    }

    public String getLastUserName() {
        return lastUserName;
    }

    public void setLastUserName(String lastUserName) {
        this.lastUserName = lastUserName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDateAsString() {
        if (null != birthDate){
            return Util.convertFormDateToString(birthDate);
        }
        return null;
    }

    public void setBirthDate(String birthDateAsString) {
        this.birthDate = Util.convertToFormDate(birthDateAsString);
    }

    @Override
    public String toString() {
        return "TestUser{" +
                "honorific=" + honorific +
                ", loginName='" + loginName + '\'' +
                ", firstUserName='" + firstUserName + '\'' +
                ", middleUserName='" + middleUserName + '\'' +
                ", lastUserName='" + lastUserName + '\'' +
                ", age=" + age +
                ", isActive=" + isActive +
                ", password='" + password + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}