package le.tafb.utils.entities.enums;

/**
 * Represents possible user honorifics.
 */
public enum Honorific
{
    SUDAR,
    SUDARYNJA,
    BARY6NJA
}