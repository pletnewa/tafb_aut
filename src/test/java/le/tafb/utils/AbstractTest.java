package le.tafb.utils;

import le.tafb.utils.data.URLsHolder;
import le.tafb.utils.db.DbUtil;
import le.tafb.utils.helpers.Nav;
import le.tafb.utils.tools.Log;
import le.tafb.utils.tools.TestListener;
import org.testng.annotations.*;

/**
 * Base test class containing common pre-/post-conditions steps and auxiliary methods.<br/>
 *
 * TODO: use listeners to track test failures like IOException. <br/>
 * TODO: log additional info like IP etc before test start.     <br/>
 */
@Listeners (TestListener.class)
public abstract class AbstractTest {

	/**
	 * Performs precondition action(s) before each test start.
	 * <p>
	 */
	@BeforeClass
	public void initTest(){
        Log.logInfo("Cleaning up the DB.");
        DbUtil.getInstance().cleanAll();

        Browser.getBrowser();

        Log.logInfo("Getting to Main page.");
        Nav.toURL(URLsHolder.getHolder().getPageMain());

        Log.logTestStart();
	}

	/**
	 * Performs post-condition action(s) after each class finish.
	 * <p>
	 */


	@AfterClass
	public void tearDownTest(){
		//Close the browser
        Browser.quit();
	}
}