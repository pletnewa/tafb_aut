package le.tafb.pages;

import le.tafb.utils.entities.TestUser;
import le.tafb.utils.helpers.UiHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PageObject class for "CreateUser" page.
 */
public class CreateUserPage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(CreateUserPage.class);

    /**
     * ById locator for Honorific selector.
     */
    private static final By BY_SLCT_HONORIFIC = new By.ById("honorific");

    /**
     * ById locator for loginName field.
     */
    private static final By BY_TB_LOGINNAME = new By.ById("loginName");

    /**
     * ById locator for firstUserName field.
     */
    private static final By BY_TB_FIRSTNAME = new By.ById("firstUserName");

    /**
     * ById locator for middleUserName field.
     */
    private static final By BY_TB_MIDDLENAME = new By.ById("middleUserName");

    /**
     * ById locator for lastUserName field.
     */
    private static final By BY_TB_LASTNAME = new By.ById("lastUserName");

    /**
     * ById locator for age field.
     */
    private static final By BY_TB_AGE = new By.ById("age");

    /**
     * ById locator for password field.
     */
    private static final By BY_TB_PASSWORD = new By.ById("psswd");

    /**
     * ById locator for birth date field.
     */
    private static final By BY_TB_BIRTHDATE = new By.ById("dateId");

    public CreateUserPage() {
    }

    /**
     * Fills in the form with values from the user object's fields.
     * NOTE: only required fields are supported.
     * TODO: add support for all fields (honorific etc)
     *
     * @param user - TestUser entity to be used as  source.
     */
    public void fillInTheForm(TestUser user) {
        sendKeysIfNotNull(BY_TB_LOGINNAME, user.getLoginName());
        sendKeysIfNotNull(BY_TB_FIRSTNAME, user.getFirstUserName());
        sendKeysIfNotNull(BY_TB_LASTNAME, user.getLastUserName());
        sendKeysIfNotNull(BY_TB_PASSWORD, user.getPassword());
        sendKeysIfNotNull(BY_TB_BIRTHDATE, user.getBirthDateAsString());
    }

    public String getValueFromLoginNameInput() {
        WebElement inputLoginName = UiHelper.findElement(BY_TB_LOGINNAME);
        return inputLoginName.getAttribute("value");
    }

    public String getValueFromFirstNameInput() {
        WebElement inputFirstName = UiHelper.findElement(BY_TB_FIRSTNAME);
        return inputFirstName.getAttribute("value");
    }
public String getValueFromLastNameInput() {
        WebElement inputLastName = UiHelper.findElement(BY_TB_LASTNAME);
        return inputLastName.getAttribute("value");
    }
public String getValueFromPasswordInput() {
        WebElement inputPassword = UiHelper.findElement(BY_TB_PASSWORD);
        return inputPassword.getAttribute("value");
    }
public String getValueFromBirthDateInput() {
        WebElement inputBirthDate = UiHelper.findElement(BY_TB_BIRTHDATE);
        return inputBirthDate.getAttribute("value");
    }

    private void sendKeysIfNotNull(By locator, String valueToSend) {
        if (null != valueToSend) {
            UiHelper.sendKeys(locator, valueToSend);
        } else {
            logger.warn(String.format("Cannot send keys to %s, the value is null", locator));
        }
    }
}