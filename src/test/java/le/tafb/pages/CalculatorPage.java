package le.tafb.pages;

import le.tafb.utils.helpers.UiHelper;
import le.tafb.utils.tools.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class CalculatorPage {

    protected static final By BY_LNK_CALC = new By.ByXPath("//a[contains(text(),\"Calc\")]");

    protected static final By BY_BTN_COMPUTE = new By.ById("computeButton");

    protected static final By BY_CELL_RESULT = new By.ById("tbResults");


     public CalculatorPage(){

     }

    /**
     * Открывает страницу Calculator
     */
    public void tryToGetToCalcPage() {
        Log.logInfo("Clicking Calc.");
        UiHelper.click(BY_LNK_CALC);

    }

    // using = "textField" - this is OK till that ID is only needed here, at 1 place.
    @FindBy(how = How.ID, using = "textfield")
    private WebElement tbFirstColumn;

    @FindBy(how = How.ID, using = "textfield_0")
    private WebElement tbSecondColumn;



    /**
     * Types values in the first and second cells and clicks 'Compute' button.
     *
     * @param firstColumn  - name to use for login.
     * @param secondColumn - password to use.
     */
    public void performOperationWith(String firstColumn, String secondColumn) {

        UiHelper.sendKeys(tbFirstColumn, firstColumn);
        UiHelper.sendKeys(tbSecondColumn, secondColumn);
        UiHelper.click(BY_BTN_COMPUTE);
        Log.logInfo("Clicking Compute");
    }

    /**
     * получает результат произведённой операции
     */
    public String getResult(){

       String result = UiHelper.getAttributeValue(BY_CELL_RESULT, "value");
        return result;
        }
    }

