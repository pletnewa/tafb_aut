package le.tafb.pages;

import le.tafb.utils.helpers.UiHelper;
import le.tafb.utils.tools.Log;
import org.openqa.selenium.By;

public class AboutPage {

    protected static final By BY_LNK_ABOUT = new By.ById("to_page_About");

    protected static final By BY_NUMBER_OF_TIMES = new By.ByXPath("//div[@id=\"zone\"]//strong");

    protected static final By BY_INCREMENT_VIA_AJAX = new By.ById("incrementAjax");

    protected static final By BY_LNK_RESET_THE_COUNTER = new By.ByXPath("//a[contains(text(), \"Reset the counter\")]");

    /**
     * Открывает страницу About
     */
    public void tryToGetToAboutPage() {

        UiHelper.click(BY_LNK_ABOUT);

    }

    /**
     * @return number of times link has been clicked
     */
    public String getNumberOfTimesClicked() {
        return UiHelper.getText(BY_NUMBER_OF_TIMES);
    }

    /**
     * clicking on the "Increment(via Ajax) link"
     */
    public void clickOnIncrementViaAjaxLink() {
        UiHelper.click(BY_INCREMENT_VIA_AJAX);
    }

    /**
     * clicking on the "Reset the counter link"
     */
    public void clickResetCounterLink() {
        UiHelper.click(BY_LNK_RESET_THE_COUNTER);
    }

}
