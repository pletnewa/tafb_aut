package le.tafb.pages;

import le.tafb.utils.Browser;
import le.tafb.utils.helpers.UiHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PageObject class for "Login" page.<br/>
 * Optimized for <a href="http://code.google.com/p/selenium/wiki/PageFactory">PageFactory</a> usage.
 */
public class LoginPage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

	public LoginPage(WebDriver driver) {
		super(driver);
	}

    // using = "loginName" - this is OK till that ID is only needed here, at 1 place.
    @FindBy(how = How.ID, using = "loginName")
    private WebElement tbLoginName;

    @FindBy(how = How.ID, using = "psswd")
    private WebElement tbPassword;

    /**
     * Types in loginName and password and submits the login form.
     * @param loginName - name to use for login.
     * @param password - password to use.
     * @return - HomePage PageObject.
     */
    public HomePage loginAs(String loginName, String password) {
        logger.debug(String.format("Logging as %s/%s", loginName, password));

            UiHelper.sendKeys(tbLoginName, loginName);
            UiHelper.sendKeys(tbPassword, password);
            submit();

         return PageFactory.initElements(Browser.getDriver(), HomePage.class);
    }

    /**
     * Checks whether Login Name field present on current page.
     *
     * @return True if it is.
     */
    public boolean isLoginNameFieldPresent(){
        return UiHelper.isPresent(By.id("loginName"));
    }

    /**
     * Checks whether Password field present on current page.
     *
     * @return True if it is.
     */
    public boolean isPasswordFieldPresent(){
        return UiHelper.isPresent(By.id("psswd"));
    }
}