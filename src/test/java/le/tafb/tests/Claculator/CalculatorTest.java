package le.tafb.tests.Claculator;

import le.tafb.pages.CalculatorPage;
import le.tafb.utils.AbstractTest;
import le.tafb.utils.Browser;
import le.tafb.utils.tools.Verify;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorTest extends AbstractTest {


    @DataProvider(name = "data")
    public Object[][] getData() {
        Object[][] data = new Object[][]{
                {"7", "9"},
                {"0", "4"}
        };

        return data;
    }

    @Test(enabled = true, dataProvider = "data", description = "Valid addition operations")
    public void testValidAdditionOperation(String value1, String value2) {
        CalculatorPage calculatorPage = PageFactory.initElements(Browser.getDriver(), CalculatorPage.class);
        calculatorPage.tryToGetToCalcPage();
        calculatorPage.performOperationWith(value1, value2);
        String resultOfAddition = calculatorPage.getResult();
        double value = Double.parseDouble(value1) + Double.parseDouble(value2);
        Verify.Equals(value, Double.parseDouble(resultOfAddition), "result of addition is incorrect");
    }


}
