package le.tafb.tests.aboutPage;

import le.tafb.pages.AboutPage;
import le.tafb.utils.AbstractTest;
import le.tafb.utils.Browser;
import le.tafb.utils.tools.Log;
import le.tafb.utils.tools.Verify;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class AboutTest extends AbstractTest {

    @BeforeClass
    public void openAboutPage(){
        AboutPage aboutPage = PageFactory.initElements(Browser.getDriver(), AboutPage.class);
        aboutPage.tryToGetToAboutPage();
    }

    @Test(enabled = true, description = "Clicking on increment (via Ajax) link")
    public void testToIncrementViaAjaxLink() {
        AboutPage aboutPage = new AboutPage();
        Log.logStep("About page is opened");
        String numberBeforeClicking = aboutPage.getNumberOfTimesClicked();    //retrieves count of clicking
        int countBeforeClicking = Integer.parseInt(numberBeforeClicking);
        Log.logStep("number of clicks is 0");
        aboutPage.clickOnIncrementViaAjaxLink();     //click on "Increment(via Ajax) link"
        String numberAfterClicking = aboutPage.getNumberOfTimesClicked();
        Log.logStep("Find new element");
        int countAfterClicking = Integer.parseInt(numberAfterClicking);
        Verify.Equals(countBeforeClicking+1,countAfterClicking, "Counts are not equal");

      }

    @Test(enabled = true, description = "testing of clicking of 'Reset the counter' link")
    public void testOfClickingOnTheResetCounterLink() {
        AboutPage aboutPage = new AboutPage();
        aboutPage.clickOnIncrementViaAjaxLink();
        aboutPage.clickResetCounterLink();
        String numberAfterReset = aboutPage.getNumberOfTimesClicked();
        int countAfterReset = Integer.parseInt(numberAfterReset);
        Verify.Equals(0,countAfterReset, "Count after reset is not 0");

    }


}
