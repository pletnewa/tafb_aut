package le.tafb.tests.user;

import le.tafb.pages.CreateUserPage;
import le.tafb.pages.EditUserPage;
import le.tafb.pages.HomePage;
import le.tafb.utils.AbstractAutoLoginTest;
import le.tafb.utils.data.URLsHolder;
import le.tafb.utils.entities.TestUser;
import le.tafb.utils.tools.Log;
import le.tafb.utils.tools.Util;
import le.tafb.utils.tools.Verify;
import le.tafb.pages.UsersPage;
import org.testng.annotations.Test;

/**
 * A container for User entity related tests.<br/>
 *
 * @author L.E.
 * @since 2013-08-06
 */
public class UserTest extends AbstractAutoLoginTest{

    /**
     * The tests verifies whether it is possible to add a new user from Home page.
     * Note: only required fields are filled in.
     * //TODO: Finish the test (fill in all the fields etc), update Requirements section and remove "enabled = false" after that.
     * <p>
     * Requirements:
     * <ul>
     *     <li>2.1</li>
     *     <li>2.4.2</li>
     *     <li>2.4.3</li>
     * </ul>
     * </p>
     * <p>
     * Scenario:
     * <ul>
     *     <li>Clean up th DB(no users except un-countable 'admin');
     *     <li>Login as admin;
     *     <li>Click "Add new user" on Home page;
     *     <li>Fill in the form(only required fields) and submit the form;
     *     <li>{Verify} Total users count has been increased (+1);
     *     <li>{Verify} There is corresponding record in the Users table;
     *     <li>Click to view the record;
     *     <li>{Verify} the form shows saved new user's data.
     * </ul>
     * </p>
     * ("enabled = false" below means that this test won't be executed even if you explicitly point to it since the test is ignored.)
     */
    @Test(enabled = true, groups={"user"}, description="Add new user from Home page.")
    public void testAddNewUserFromHomePage_OnlyRequiredFields(){
        //DB clean-up and logging as an admin is made in base class(es) in @BeforeMethod method(s).

        Log.logStep("Click 'Add new user' on Home page;");
        //HomePage homePage = PageFactory.initElements(Browser.getDriver(), HomePage.class);

        //Assuming pre-condition methods worked well, we are on the "Home" page at the moment.
        //However, let's check one more time. //TODO: User Assert here.
        Verify.isCurrentUrlContainsExpected(URLsHolder.getHolder().getPageHome(), "Current page is not 'Home'");
        CreateUserPage createUserPage = new HomePage().gotoCreateNewUser();

        String loginName = Util.getSomeVeryShortValue();
        String firstUserName = Util.getSomeVeryShortValue();
        String lastUserName = Util.getSomeShortValue();
        String password = Util.getSomeValue();
        String birthDate = "2001-01-14";


        //Create and fill in "user-to-create" object.
        TestUser userToCreate = new TestUser(
                loginName,
                firstUserName,
                lastUserName,
                password,
                birthDate);
        Log.logInfo("User-to-create: " + userToCreate);

     Log.logStep("Fill in the form(only required fields) and submit the form;");
        createUserPage.fillInTheForm(userToCreate);

        createUserPage.submit();

       // Assert.fail("//TODO: Finish this.");
        //TODO: Finish this.
        //{Verify} Total users count has been increased (+1);

        UsersPage usersPage = new UsersPage();
        int actualTotal = new UsersPage().getTotalRecords();
        Verify.Equals(1, actualTotal, "Unexpected number of users");
        Log.logStep("Verify that 1 is added to the database");
        String userLoginFromTable = usersPage.getUserLoginNameFromTable(1);

        //{Verify} There is corresponding record in the Users table;
        Verify.Equals(userLoginFromTable, loginName, "There is no" + loginName + " record in the Users table");

       //  <li> Click to view the record;
          usersPage.editUser(loginName);
       //  <li>{Verify} the form shows saved new user's data.

        EditUserPage editUserPage = new EditUserPage();
        Verify.Equals(loginName, editUserPage.getValueFromLoginNameInput(), "InputLoginName from Edit page is different from entered one" );
        Verify.Equals(firstUserName, editUserPage.getValueFromFirstNameInput(), "InputFirstName from Edit page is different from entered one" );
        Verify.Equals(lastUserName, editUserPage.getValueFromLastNameInput(), "InputLastName from Edit page is different from entered one" );
        Verify.Equals(password, editUserPage.getValueFromPasswordInput(), "InputPassword from Edit page is different from entered one" );
        Verify.Equals(birthDate, editUserPage.getValueFromBirthDateInput(), "InputBirthDate from Edit page is different from entered one" );
    }
}