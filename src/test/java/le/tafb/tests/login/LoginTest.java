package le.tafb.tests.login;

import le.tafb.pages.HomePage;
import le.tafb.pages.LoginPage;
import le.tafb.utils.AbstractTest;
import le.tafb.utils.Browser;
import le.tafb.utils.data.Constants;
import le.tafb.utils.data.URLsHolder;
import le.tafb.utils.tools.Log;
import le.tafb.utils.tools.Verify;
import org.openqa.selenium.support.PageFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A container class for login/logout related methods.<br/>
 *
 * @author L.E.
 * @since 2012-11-25
 */
public class LoginTest extends AbstractTest {

    @DataProvider(name = "data")
    public Object[][] getData() {
        Object[][] data = new Object[][]{
                {"Invalid", "some pass"},
                {"Invalid", "password"}
        };

        return data;
    }

    @Test(dataProvider = "data")
    public void invalidLogin(String loginName, String password) {
        LoginPage loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);
        Log.logStep("Login ");
        HomePage homePage = loginPage.loginAs(loginName, password);

        Log.logStep("{Verify} Current page is Index/Login page;");
        Verify.True(loginPage.isLoginNameFieldPresent(), "There is no LoginName field, probably current page is not Login/Index");
        Verify.True(loginPage.isPasswordFieldPresent(), "There is no Password field, probably current page is not Login/Index");
    }


    /**
     * Performs logging in using valid admin credentials.
     * Checks whether login successful.
     * Performs logout.
     * Checks whether logout successful.<br/>
     * Note: The DB is cleaned up in parent's @BeforeMethod.
     * <p>
     * Requirements:
     * <ul>
     * <li>1.1</li>
     * <li>1.6</li>
     * </ul>
     * </p>
     * <p>
     * Scenario:
     * <ul>
     * <li>Open AUT Index/Login page (is performed in @see AbstractTest);
     * <li>Init Login Page model;
     * <li>Type in valid name/password and submit the form;
     * <li>{Verify} Current page is the Home page;
     * <li>Click LogOut;
     * <li>{Verify} Current page is Index/Login page;
     * </ul>
     * </p>
     */
    @Test(groups = {"login"}, description = "Login/logout with  valid credentials")
    public void testLoginAsAdminLogout() {
        //See http://code.google.com/p/selenium/wiki/PageFactory for PageFactory description.
        LoginPage loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);

        Log.logStep("Login ");
        //TODO: username/pwd should be specified as properties.

        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("loginTest.properties");

            String loginName = properties.getProperty(Constants.LOGIN_TEST_PROP_KEY_LOGIN_NAME);
            String loginPassword = properties.getProperty(Constants.LOGIN_TEST_PROP_KEY_LOGIN_PASSWORD);
            HomePage homePage = loginPage.loginAs(loginName, loginPassword);

            Log.logStep("{Verify} Current page is the Home page;");
            String expectedURL = URLsHolder.getHolder().getPageHome();
            Verify.isCurrentUrlContainsExpected(expectedURL, "Wrong URL after logging in.");
            System.out.println("Я включился"); //печать в строку
            Log.logStep("Click LogOut;");
            loginPage = homePage.logout();

            Log.logStep("{Verify} Current page is Index/Login page;");
            Verify.True(loginPage.isLoginNameFieldPresent(), "There is no LoginName field, probably current page is not Login/Index");

            Verify.True(loginPage.isPasswordFieldPresent(), "There is no Password field, probably current page is not Login/Index");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}


//Properties properties = new Properties();
//File propertyFile = new File(Login.properties);
//properties.load(new FileReader (propertyFile));
//String userNameID = properties.getProperty("field.username,id");
//String userNameID = properties.getProperty("field.username,id");