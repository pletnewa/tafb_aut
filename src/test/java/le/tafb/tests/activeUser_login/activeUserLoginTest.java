package le.tafb.tests.activeUser_login;

import le.tafb.pages.HomePage;
import le.tafb.pages.LoginPage;
import le.tafb.utils.AbstractTest;
import le.tafb.utils.Browser;
import le.tafb.utils.data.URLsHolder;
import le.tafb.utils.tools.Log;
import le.tafb.utils.tools.Verify;
import org.junit.Before;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.internal.FindsByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import le.tafb.utils.db.DbUtil;
import org.openqa.selenium.WebDriver;
import le.tafb.pages.BasePage;


public class activeUserLoginTest extends AbstractTest {

//    @Before public void performsLogOut(){
//        LoginPage loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);
//        BasePage basePage = new BasePage();
//        basePage.logout();
//        FirefoxDriver driver = new FirefoxDriver();
//        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
//    }

    @Test(description = "Active users shall be able to login")
    public void testLoginOnlyAsActiveUser() {
        DbUtil insertUserToDatabase = new DbUtil();
        insertUserToDatabase.insertStandardUserEntries();
        LoginPage loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);
        String loginNameOfActiveUser = "Active";
        String loginPasswordOfActiveUser = "psw1d";
        HomePage homePage = loginPage.loginAs(loginNameOfActiveUser,loginPasswordOfActiveUser);
        String expectedURL = URLsHolder.getHolder().getPageHome();
        Verify.isCurrentUrlContainsExpected(expectedURL, "Wrong URL after logging in.");

    }

    @Test(description = "Inactive users are not able to login")
    public void testInactiveUserIsNotAbleToLogin() {
        DbUtil insertUserToDatabase = new DbUtil();
        insertUserToDatabase.insertStandardUserEntries();
        LoginPage loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);
        String loginNameOfInactiveUser = "Inactive";
        String loginPasswordOfInactiveUser = "psw3d";
        HomePage homePage = loginPage.loginAs(loginNameOfInactiveUser, loginPasswordOfInactiveUser);
        Verify.True(homePage.isWrongLoginErrorIsPresent(),"Invalid user is able to log in");
    }

}
